# **Welkom bij de uitleg voor installatie van argocd op Azure AKS**

Voor installatie van ArgoCD is een Azure AKS cluster benodigd.

### **Azure AKS cluster creatie**

Open portal.azure.com en open 'kubernetes service':
![](2023-03-17-16-42-26.png)

_Creeër een nieuw cluster:_
![](2023-03-17-16-44-12.png)

![](2023-03-17-16-44-55.png)
Vul hier de volgende onderdelen in:

**_Resourcegroup_:** Vul de resourcegroup in waarin het nieuwe cluster in wordt geconfigureerd.

**_Cluster Preset configuration:_** In dit geval kan Dev/test worden gebruikt omdat het om een testomgeving gaat.

##### **Let op!** Dit is voldoende voor de test omgeving. Er kan worden gekozen voor andere instellingen bij een productie omgeving
<br>

Bij het onderdeel Networking:
![](2023-03-17-16-50-56.png)

**_Enable HTTP aplication routing:_** Kan aangezet worden om dns namen te laten koppelen aan alle pods. (eigen keuze)

##### **Let op!** Voor de andere onderdelen zijn geen aanpassingen benodigd.
<br>
Klik op review en create en creeër het cluster.
<br><br>

### **Argocd**
1. Begin met het maken van een namespace. Dit doe je via het commando: `kubectl create namespace argocd`
2. Vervolgens kan ArgoCD geïnstalleerd worden. Gebruik hiervoor het commando: `kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml`
3. Controleer vervolgens met het commando: `kubectl get all -n argocd` en controleer of alles toegevoegd is.
4. Vervolgens willen we ArgoCD toegankelijk maken van buitenaf. Daarvoor moet “ClusterIP” omgezet worden naar “NodePort”. Gebruik hiervoor de commando: `kubectl -n argocd edit svc argocd-server`
5. Controleer vervolgens via `kubectl get all -n argocd` of de service correct is omgezet.
6. Gebruik het commando `kubectl expose deployment argocd-server --type=LoadBalancer --name=loadbalancerargocd --namespace =argocd` om argocd te configureren naar het internet.
7. Vervolgens kan je navigeren naar de site <ip>:<8080> (controleer met `kubectl get services –namespace=argocd`, hier de naam van loadbalancer opzoeken. (in dit geval loadbalancerargocd)
8. met het commando `kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d`
kan het admin wachtwoord opgevraagd worden wat nodig is om in te loggen. Hierbij is de gebruikersnaam admin.

### Guestbook installatie op argocd.

Maak een nieuwe app aan:
_klik op new app:_ <br>
![](2023-03-17-16-53-05.png)

Vul in als volgt: <br>
![](![img_6.png](img_6.png).png)

vul de volgende url in bij repository url: `https://github.com/argoproj/argocd-example-apps.git`
![](![img_7.png](img_7.png).png)

Vul bij destination de volgende info in: <br>
![](![img_8.png](img_8.png).png)

Klik op create:<br>
![](![img_9.png](img_9.png).png)

Nu moet de app gedeployed (gesynced) worden. 

Dit wordt gedaan door middel van op de sync knop te drukken:
![](2023-03-17-17-11-47.png)
Nu wordt de app deployed. dit kan gecontroller worden met het commando: `kubectl get deployments`

Hier wordt het volgende weergegeven:
![](![img_12.png](img_12.png).png)

Nu kan door middel van het commando: `kubectl expose deployment guestbook-ui --type=LoadBalancer --name=loadbalancerguestbook`
<br>
<br>
<br>
Gefeliciteerd! U heeft nu een app (guestbook) op Argocd opgestart door middel van kubernetes op Azure!


